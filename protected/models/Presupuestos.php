<?php

/**
 * This is the model class for table "presupuestos".
 *
 * The followings are the available columns in table 'presupuestos':
 * @property integer $id
 * @property string $numero_proceso
 * @property string $descripcion
 * @property string $fecha_creacion
 * @property integer $sede
 * @property string $presupuesto
 */
class Presupuestos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'presupuestos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
                        array('numero_proceso','unique'),
			array('descripcion', 'required'),
			array('sede', 'numerical', 'integerOnly'=>true),
			array('numero_proceso', 'length', 'max'=>8),
			array('presupuesto', 'length', 'max'=>10),
                        array('fecha_creacion','default','value'=>new CDbExpression('NOW()'),'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numero_proceso, descripcion, fecha_creacion, sede, presupuesto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numero_proceso' => 'Numero Proceso',
			'descripcion' => 'Descripcion',
			'fecha_creacion' => 'Fecha Creacion',
			'sede' => 'Sede',
			'presupuesto' => 'Presupuesto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero_proceso',$this->numero_proceso,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('fecha_creacion',$this->fecha_creacion,true);
		$criteria->compare('sede',$this->sede);
		$criteria->compare('presupuesto',$this->presupuesto,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Presupuestos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        
    public static function Sede() {

        return array(0 => 'Bogotá ', 1 => 'México',2=>'Perú');
    }
    public static function getsede($sede) {

        switch ($sede):
            case 0:  $valor = 'Bogotá'; break;
            case 1:  $valor = 'México'; break;
            case 2:  $valor = 'Perú'; break;
        endswitch;
        
        return $valor;
        
    }
    
    public static function getdolares($presupuesto){
        
        $valor_dolar = 2871.50;
        $dolar = $presupuesto * $valor_dolar;
        return Yii::app()->numberFormatter->formatCurrency($dolar,"US");
        
    }
}
