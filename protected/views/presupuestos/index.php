<?php
/* @var $this PresupuestosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Presupuestoses',
);

$this->menu=array(
	array('label'=>'Create Presupuestos', 'url'=>array('create')),
	array('label'=>'Manage Presupuestos', 'url'=>array('admin')),
);
?>

<h1>Presupuestoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
