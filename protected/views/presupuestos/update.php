<?php
$this->menu=array(	
	array('label'=>'Crear Presupuestos', 'url'=>array('create')),
	array('label'=>'Detalle Presupuestos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Listar Presupuestos', 'url'=>array('admin')),
);
?>

<h1>Editar Numero de Presupuesto <?php echo $model->numero_proceso; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>