<?php

$this->menu=array(	
	array('label'=>'Crear Presupuesto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#presupuestos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Lista de  Presupuesto</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'presupuestos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(		
		'numero_proceso',
		'descripcion',
                array('name' => 'fecha_creacion', 'type' => 'raw', 'value' => 'date("d/m/y h:i A", strtotime($data->fecha_creacion))', 'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array('model' => $model, 'attribute' => 'fecha_creacion'), true)),
                array('name'=>'sede','type'=>'text','value'=> 'Presupuestos::getsede($data->sede)'),
		array('name'=>'presupuesto','type'=>'raw','value'=>'Yii::app()->numberFormatter->formatCurrency($data->presupuesto,"CO")'),
		array('name'=>'presupuesto','header'=>'Equivalente a Dolar','type'=>'raw','value'=>'Presupuestos::getdolares($data->presupuesto)'),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
