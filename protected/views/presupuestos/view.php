<?php
$this->menu=array(	
	array('label'=>'Crear Presupuestos', 'url'=>array('create')),
	array('label'=>'Editar Presupuestos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Eliminar Presupuestos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Lista Presupuestos', 'url'=>array('admin')),
);
?>

<h1>Detalle Presupuestos #<?php echo $model->numero_proceso; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'numero_proceso',
		'descripcion',
		'fecha_creacion',
		'sede',
		'presupuesto',
	),
)); ?>
