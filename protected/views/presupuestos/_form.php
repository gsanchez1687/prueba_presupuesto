<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'presupuestos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <?php if (!$model->isNewRecord): ?>
	  <div class="row">
		<?php echo $form->labelEx($model,'numero_proceso'); ?>
		<?php echo $form->textField($model,'numero_proceso',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'numero_proceso'); ?>
 	  </div>
        <?php endif; ?>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sede'); ?>
		 <?php echo $form->dropDownList($model, 'sede', Presupuestos::Sede(), array('empty' => 'Seleccione...', 'class' => 'form-control')); ?>
		<?php echo $form->error($model,'sede'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'presupuesto'); ?>
		<?php echo $form->textField($model,'presupuesto',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'presupuesto'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->