<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
           
		$user = Usuarios::model()->find(array(
                   'condition' => "t.username =:username",
                   'params' => array(':username'=>strtolower($this->username))   
                ));
                                    
                var_dump($user);

      
        if ($user === null){
            
            Yii::app()->user->setFlash('success', "La cuenta a la que intentas ingresar no existe");
        }
        
        else if (!$user->validatePassword($this->password)){
           Yii::app()->user->setFlash('success', "La contraseña es incorrecta");       
           
        } else if($user->estado_id == '9'){
            
            Yii::app() -> user->setFlash('success', "Usuario Inactivo");
	    $this -> errorCode = self :: ERROR_PASSWORD_INVALID;
            
        }  else if($user->estado_id == '10' ) {
            
            Yii::app() -> user->setFlash('success', "Usuario Suspendido");
            $this -> errorCode = self :: ERROR_PASSWORD_INVALID;
        }
        
        else {
             
          
            $this->setState('username', $user->username);
            $this->setState('rol', $user->rol_id);
            $this->setState('nombre', $user->nombre);                       
                                 
            $this->username = $user->username;
           
            $this->errorCode = self::ERROR_NONE;
        }
        return $this->errorCode == self::ERROR_NONE;
	}
}